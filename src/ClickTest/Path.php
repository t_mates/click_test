<?php
namespace Makarenkov\ClickTest;

/**
 * please don`t move this file
 * because entire app use it for
 * app root detecting
 */
class Path
{
    public static function base($extendedPath = '')
    {
        $currentDirDeepLevelFromRoot = 2;
        $dir = __DIR__;
        foreach (range(0, $currentDirDeepLevelFromRoot - 1) as $value) {
            $pos = mb_strrpos($dir, DIRECTORY_SEPARATOR);
            $dir = mb_substr($dir, 0, $pos);
        }
        if ($extendedPath !== '') {
            $extendedPath = DIRECTORY_SEPARATOR . $extendedPath;
        }
        return $dir . $extendedPath;
    }
}