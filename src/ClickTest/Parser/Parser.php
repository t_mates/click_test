<?php
namespace Makarenkov\ClickTest\Parser;

use Makarenkov\ClickTest\Model\Geodata;
use PDOException;
use SplFileObject;
use Makarenkov\ClickTest\Path;
use Makarenkov\ClickTest\Db;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

class Parser
{
    protected $extension;
    protected $defaultPath;
    protected $currentPath;

    public function __construct($extension = 'csv')
    {
        $this->extension = $extension;
        $this->defaultPath = Path::base('output');
        $this->currentPath = $this->defaultPath;
    }

    public function setDirectory($path)
    {
        $this->currentPath = $path;
        return $this;
    }

    public function parse()
    {
        $files = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($this->currentPath, RecursiveDirectoryIterator::SKIP_DOTS),
            RecursiveIteratorIterator::CHILD_FIRST
        );
        foreach ($files as $fileinfo) {
            if ($fileinfo->isFile()) {
                $this->parseFile($fileinfo->getPathname());
            }
        }
    }

    protected function parseFile($path)
    {
        $file = new SplFileObject($path);
        foreach ($file as $line_num => $line) {
            if ( ! $file->eof()) {
                $arrayLine = explode(',', $line);
                $isValidLine = isset($arrayLine[0]) && 
                                isset($arrayLine[1]) && 
                                isset($arrayLine[2]) && 
                                isset($arrayLine[3]) && 
                                isset($arrayLine[4]);
                if ($isValidLine) {
                    list($date, $geo, $zone, $impressions, $revenue) = $arrayLine;
                    try {
                        $this->createOrUpdate($date, $geo, $zone, $impressions, $revenue);
                    } catch (PDOException $e) {
                        // 
                    }
                }
            }
        }
    }

    protected function createOrUpdate($date, $geo, $zone, $impressions, $revenue)
    {
        $geodata = new Geodata;
        $rows = $geodata->find($date, $geo, $zone);
        if (count($rows) > 0) {
            $newImpressions = $rows[0][3] + $impressions;
            $newRevenue = (float)$rows[0][4] + (float)$revenue;
            $geodata->update($date, $geo, $zone, $newImpressions, $newRevenue);
        } else {
            $geodata->insert($date, $geo, $zone, $impressions, $revenue);
        }
    }
}