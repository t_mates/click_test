CREATE TABLE public.geodata (
    "date" date NOT NULL,
    geo varchar NOT NULL,
    "zone" int4 NOT NULL,
    impressions int4 NOT NULL,
    revenue numeric(9,2) NOT NULL,
    CONSTRAINT geodata_un_date_geo_zone UNIQUE (geo, date, zone)
);
