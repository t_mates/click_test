<?php

require __DIR__ . DIRECTORY_SEPARATOR . 
        '..' . DIRECTORY_SEPARATOR . 
        'vendor' . DIRECTORY_SEPARATOR . 
        'autoload.php';

use Makarenkov\ClickTest\Path;
use Makarenkov\ClickTest\Bootstrapper;
use Makarenkov\ClickTest\Facade\Parser;

try {
    Bootstrapper::load();
    Parser::make()->setDirectory($argv[1] ?? Path::base('output'))->parse();
} catch (Exception $e) {
    echo $e;
}