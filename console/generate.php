<?php

require __DIR__ . DIRECTORY_SEPARATOR . 
        '..' . DIRECTORY_SEPARATOR . 
        'vendor' . DIRECTORY_SEPARATOR . 
        'autoload.php';

use Makarenkov\ClickTest\Facade\Generator;
use Makarenkov\ClickTest\Bootstrapper;
use Makarenkov\ClickTest\Exception\FolderExistsException;

try {
    Bootstrapper::load();
    Generator::make()->generate();
} catch (FolderExistsException $e) {
    echo $e;
} catch (Exception $e) {
    echo $e;
}